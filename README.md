# JavaEE StartUp

## To Download Airhack JavaEE8 Essentials Archetype:<br>
`mvn archetype:generate -Dfilter=com.airhacks:javaee8-essentials-archetype`<br>
### Reference:
- [JavaEE GitHub](https://github.com/AdamBien/javaee8-essentials-archetype)
- [JavaEE Maven_Reposiory](https://mvnrepository.com/artifact/com.airhacks/javaee8-essentials-archetype)

<br>

## To Download Airhack JakartaEE Essentials Archetype:<br>
`mvn archetype:generate -Dfilter=com.airhacks:jakartaee-essentials-archetype`<br>
### Reference:
- [JakartaEE GitHub](https://github.com/AdamBien/JakartaEE-essentials-archetype)
- [JakartaEE Maven Reposiory](https://mvnrepository.com/artifact/com.airhacks/jakartaee-essentials-archetype)

<br>

## To Download Airhack MicroProfile Essentials Archetype:<br>
`mvn archetype:generate -Dfilter=com.airhacks:microprofile-essentials-archetype`<br>
### Reference:
- [MicroProfile GitHub](https://github.com/AdamBien/microprofile-essentials-archetype)
- [MicroProfile Maven_Reposiory](https://mvnrepository.com/artifact/com.airhacks/microprofile-essentials-archetype)

<br>

## Wildfly:
- Java Version: 8
- Maven Version: 3.8
- Deployment Path: wildfly:/opt/jboss/wildfly/standalone/deployments
- Port: 8080
- URIs:
    - /javaee/api/v1/ping
    - /javaee-simple/api/v1/ping
    - /jakartaee/api/v1/ping
    - /jakartaee-simple/api/v1/ping
    - /microprofile/api/v1/ping
    - /helloworld/api/v1/hello

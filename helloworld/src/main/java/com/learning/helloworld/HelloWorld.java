package com.learning.helloworld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("hello")
public class HelloWorld {

    @GET
    public String message() {
        return "HelloWorld!!!";
    }

}

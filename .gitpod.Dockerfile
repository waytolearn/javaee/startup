FROM gitpod/workspace-base

RUN sudo apt-get update \
  && sudo apt-get install openjdk-8-jdk -y \
  && sudo wget https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.tar.gz -P /tmp \
  && sudo tar xzvf /tmp/apache-maven-3.8.6-bin.tar.gz -C /opt/ \
  && echo export M2_HOME='/opt/apache-maven-3.8.6' >> .bashrc \
  && echo export PATH='$PATH:$M2_HOME/bin' >> .bashrc
  
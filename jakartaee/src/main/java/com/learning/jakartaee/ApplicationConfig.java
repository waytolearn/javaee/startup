package com.learning.jakartaee;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api/v1")
public class ApplicationConfig extends Application {

}
